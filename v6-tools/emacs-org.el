
(setq load-path (cons (expand-file-name "./el/") load-path))


(setq auto-mode-alist (cons '("\\.lut$" . lutin-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.lus$" . lustre-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.rif$" . rif-mode) auto-mode-alist))


(autoload 'rif-mode "rif" "" t)
(autoload 'lutin-mode "lutin" "Edition de code lutin" t)
(autoload 'lustre-mode "lustre" "Edition de code lustre" t)

(setq org-export-html-postamble nil)  ; optionnel, pour enlever le pied de page
(setq org-html-htmlize-output-type 'inline-css) ; utilise du CSS en ligne pour la coloration
(setq org-export-with-sub-superscripts nil) ; désactive les superscripts/superscripts si tu n'en veux pas


;(require 'cl-lib)
(require 'org)
;(require 'color-theme)

;(require 'htmlize)
(require 'ob)
(require 'ob-lutin)
(require 'ob-ocaml)
(require 'ob-lustre)
;(require 'zenburn)


;; make sure that when we export in HTML, that we don't export with inline css.
;; that way the CSS of the HTML theme will be used instead which is better
(setq org-html-htmlize-output-type 'css)



(setq org-src-fontify-natively t)
(setq org-src-preserve-indentation t)

(org-babel-do-load-languages
 'org-babel-load-languages
 '((emacs-lisp . t)
   (ocaml . t)
   (lutin . t)
   (lustre . t)
   (rif . t)
   (shell . t)
   )
 )

;;; pour faire comme suggeré ici
;;; https://lists.gnu.org/archive/html/emacs-orgmode/2011-03/msg00857.html
;;; mais ca ne fonctionne pas (avec cette vrsion de org-mode)
;; (add-hook 'org-babel-tangle-body-hook
;;             (lambda () (org-export-preprocess-apply-macros)))
