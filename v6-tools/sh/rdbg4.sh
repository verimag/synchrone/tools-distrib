#!/bin/bash
echo "ni 10\nq\n" | \
rdbg -sut-nd "lv6 f.lus -n a_node" \
     -env-nd "lutin env.lut -n an_env" \
     -oracle "lv6 f.lus -n some_prop"
