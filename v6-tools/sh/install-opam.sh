export OPAMCONFIRMLEVEL=unsafe-yes # necessary for batch install test/ci

set -x
sudo apt-get update
sudo apt-get install -y make wget
wget https://raw.githubusercontent.com/ocaml/opam/master/shell/install.sh
yes '' | sudo sh install.sh || echo "ignore failure"
opam init \
           --disable-sandboxing  # only necessary when run from docker
opam switch create 4.12.0 # optional
eval $(opam env)

cp `which opam` $OPAM_SWITCH_PREFIX/bin/

opam repo add verimag-sync-repo "http://www-verimag.imag.fr/DIST-TOOLS/SYNCHRONE/opam-repository" --all-switches
opam repo add opam-repo "https://opam.ocaml.org/" --all-switches
opam update -y

opam depext lustre-v6 lutin sasa salut || echo "not needed for recent opam version"
opam install lustre-v6 lutin sasa salut
