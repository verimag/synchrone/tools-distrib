#!/bin/bash
# align user and group ids if necessary (i.e., if uid <> 1000)
# inspired from https://github.com/Frederic-Boulanger-UPS/docker-ubuntu-novnc/blob/master/rootfs/startup.sh

if [ -z "$USER" ]; then
    echo "Warning: If your user id is not 1000, you may want to run this image with"
    echo " -e USERID=\`id -u\` -e GROUPID=\`id -g\` -e USER=\`id -n -u\`"
    USER=verimag
else
    echo "* enable custom user: $USER"
    if [ -z "$PASSWORD" ]; then
	echo "  set default password to \"verimag\""
	PASSWORD=verimag
    fi
    echo "  Password set to $PASSWORD"
    UIDOPT=""
    UIDVAL=""
    GUIDOPT=""
    GUIDVAL=""
    if [ -z "$USERID" ]; then
	echo "  user id in container may not match user id on host"
    else
	echo "  setting user id to $USERID"
	UIDOPT="--non-unique --uid"
	UIDVAL=$USERID
    fi
    if [ -z "$GROUPID" ]; then
	echo "  group id in container may not match user id on host"
    else
	echo "  setting group id to $GROUPID"
	GUIDOPT="--gid"
	GUIDVAL=$GROUPID
    fi
    
    cp /home/verimag/.emacs /root/
    cp -rf /home/verimag/.emacs.d /root/
    
    groupadd --non-unique -g $GROUPID $USER-group
    useradd --create-home --skel /root --shell /bin/bash --groups adm,sudo $UIDOPT $UIDVAL $GUIDOPT $GUIDVAL $USER
    HOME=/home/$USER
    echo "$USER:$PASSWORD" | chpasswd
    cp -r /home/verimag/{.profile,.bashrc} ${HOME}
    
    # with this mv, the chown last for ages because of a overlayfs bug in 20.04
    # mv /home/verimag/.opam ${HOME}
    # nb: if ever I decomment that, I'd still need to fix the `opam env` result
    # workaround: 
    ln -s /home/verimag/.opam ${HOME}/.opam
    # $USER won't be able to update opam, but it's ok (still possible via su verimag)
    
    chown -R $USERID:$GROUPID ${HOME}
    [ -d "/dev/snd" ] && chgrp -R adm /dev/snd

# necessary after a ssh but breaks mobaxterm+windows
# export DISPLAY=:0.0
fi

if [ "$#" -eq 0 ]
    then
    exec gosu $USER bash 
else
    echo "Executing '$@' in verimag-reactive-toolbox"
    exec gosu $USER "$@"
fi
