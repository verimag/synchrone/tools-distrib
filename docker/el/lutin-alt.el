
;;; lutin.el  -  Major mode for editing lustre source in Emacs

;;; lutin-mode.el --- Major mode for the Lutin language

;;; Code:

(defvar lutin-mode-hook nil)

(defvar  lutin-mode-map
   (let ((map (make-sparse-keymap)))
     map)
   "Keymap for `lutin-mode'.")

 (defvar lutin-mode-map nil
   "Keymap for lutin major mode.")


;;; Définir les groupes de mots-clés du langage Lutin
(defconst lutin-keywords-loop-fby-raise-try-trap-catch-do
  '("loop" "fby" "raise" "try" "trap" "catch" "do")
  "Mots-clés de contrôle (loop, fby, raise, etc.).")

(defconst lutin-keywords-const-extern-node-run-include-returns-type
  '("const" "extern" "node" "run" "include" "returns" "type")
  "Mots-clés liés à la déclaration (const, extern, node, etc.).")

(defconst lutin-keywords-let-assert-exist-in-if-then-else
  '("let" "assert" "exist" "in" "if" "then" "else")
  "Mots-clés conditionnels et d'affectation (let, assert, if, etc.).")

(defconst lutin-keywords-logic-truth
  '("true" "and" "or" "not" "false")
  "Mots-clés logiques (true, false, and, or, etc.).")

(defconst lutin-keywords-types
  '("trace" "bool" "int" "ref" "real")
  "Types (trace, bool, int, etc.).")

;;; Définir la coloration syntaxique
(defconst lutin-font-lock-keywords
  (let* ((loop-fby-raise-try-trap-catch-do-regexp (regexp-opt lutin-keywords-loop-fby-raise-try-trap-catch-do 'words))
         (const-extern-node-run-include-returns-type-regexp (regexp-opt lutin-keywords-const-extern-node-run-include-returns-type 'words))
         (let-assert-exist-in-if-then-else-regexp (regexp-opt lutin-keywords-let-assert-exist-in-if-then-else 'words))
         (logic-truth-regexp (regexp-opt lutin-keywords-logic-truth 'words))
         (types-regexp (regexp-opt lutin-keywords-types 'words)))
    `((,loop-fby-raise-try-trap-catch-do-regexp . font-lock-keyword-face)           ;; Couleur pour les mots-clés de contrôle
      (,const-extern-node-run-include-returns-type-regexp . font-lock-constant-face) ;; Couleur pour les mots-clés de déclaration
      (,let-assert-exist-in-if-then-else-regexp . font-lock-variable-name-face)      ;; Couleur pour les mots-clés conditionnels
      (,logic-truth-regexp . font-lock-builtin-face)                                ;; Couleur pour les mots-clés logiques
      (,types-regexp . font-lock-type-face)))                                        ;; Couleur pour les types
  "Définition des expressions régulières pour la coloration syntaxique de Lutin.")

;;; Définir le mode majeur Lutin
(define-derived-mode lutin-titi-mode prog-mode "lutin"
  "Major mode for editing Lutin code."
  :syntax-table nil
  (setq font-lock-defaults '(lutin-font-lock-keywords))
  (use-local-map lutin-mode-map)
  (setq-local comment-start "--")
  (setq-local comment-end "")
  )

;;; Associating lutin-mode with .lutin files
(add-to-list 'auto-mode-alist '("\\.lut\\'" . lutin-mode))

;;; Fournir le mode
(provide 'lutin-titi-mode)
; (require 'lutin-mode)

;;; lutin-mode.el ends here


; (require 'font-lock)


; ; version of lutin-mode
; (defconst lutin-mode-version "0.0")


; ;;; Hooks

; (defvar lutin-mode-hook nil
;   "functions called when entering Lutin Mode.")

; ;;; Key-map for Lutin-mode

; (defvar lutin-mode-map nil
;   "Keymap for lutin major mode.")


; ;;; Font-lock -----------------------------------------------------

; (defvar lutin-font-lock-keywords nil
;   "Regular expression used by Font-lock mode.")

; (setq lutin-font-lock-keywords
;       '(
;         ("--.*$" . font-lock-comment-face)
;         ("\\<\\(loop\\|fby\\||raise\\|try\\|trap\\|catch\\|do\\)\\>" . font-lock-builtin-face)
;         ("\\<\\(const\\|extern\\|node\\|run\\|include\\|returns\\|type\\)\\>" . font-lock-keyword-face)
;         ("\\<\\(let\\|assert\\|exist\\|in\\|if\\|then\\|else\\)\\>" . font-lock-keyword-face)
;         ("\\<\\(true\\|and\\|or\\|not\\|false\\)\\>" . font-lock-reference-face)
;         ("\\<\\(trace\\|bool\\|int\\|ref\\|real\\)\\(\\^.+\\)?\\>" . font-lock-type-face)
;         ("\\<\\(pre\\)\\>" . font-lock-constant-face)
; ))



; (defun lutin-font-mode ()
;   "Initialisation of font-lock for Lutin mode."
;   (make-local-variable 'font-lock-defaults)
;   (setq font-lock-defaults
;         '(lutin-font-lock-keywords t)))

; ; font-lock isn't used if in a  console
; (if window-system
;     (prog2
; 	(add-hook 'lutin-mode-hook
; 		  'turn-on-font-lock)
; 	(add-hook 'lutin-mode-hook
; 		  'lutin-font-mode)))

;
; (defun lutin-line-is-comment (&optional arg)
;   "non-nil means line is only a commentary."
;   (interactive)
;   (save-excursion
;     (beginning-of-line arg)
;     (skip-chars-forward " \t")
;     (looking-at "--")))


; (setq comment-start "(*")
; (setq comment-end "*)")


; (setq comment-start "-- ")
; (setq comment-end "")

; ;;; Major-mode

; (defun lutin-mode ()
;   "Major mode for editing Lutin files.

;   Only keywords colaraition for the moment...
; "

;   (interactive)
;   (kill-all-local-variables)
;   (setq major-mode 'lutin-mode)
;   (setq mode-name "Lutin")
;   (use-local-map lutin-mode-map)
;   (run-hooks 'lutin-mode-hook))




; (provide 'lutin)

; ;;; lutin .el ends here...
