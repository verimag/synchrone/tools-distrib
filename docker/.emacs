(setq inhibit-startup-message t)
(add-to-list 'load-path ".")



(require 'package)

(setq package-archives
      '(
        ("gnu" . "https://elpa.gnu.org/packages/")
        ("melpa" . "https://melpa.org/packages/")
        ))
(package-initialize)

; (unless package-archive-contents   (package-refresh-contents))

(package-refresh-contents)


(message "==> package-install 'firestarter")
(package-install 'firestarter)
(message "==> package-install 'firestarter done")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Lustre langages syntax coloring
(setq load-path (cons "/home/verimag/el" load-path))

(setq auto-mode-alist (cons '("\\.lus$" . lustre-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.lut$" . lutin-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.rif$"  . rif-mode) auto-mode-alist))

(autoload 'lustre-mode "lustre" "For editing Lustre programs" t)
(autoload 'lutin-mode "lutin" "For editing Lutin programs" t)
(autoload 'rif-mode "rif" "For viewing RIF outputs" t)
(package-install 'htmlize)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; org
(setq org-src-fontify-natively t)
(setq org-src-preserve-indentation t)
(setq org-return-follows-link t)
(setq org-display-internal-link-with-indirect-buffer t)
(setq org-latex-listings t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; windows shortcut
(cua-mode t)
(setq cua-auto-tabify-rectangles nil) ;; Don't tabify after rectangle commands
(transient-mark-mode 1) ;; No region when it is not highlighted
(setq cua-keep-region-after-copy t) ;; Standard Windows behaviour

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; misc
(fset 'yes-or-no-p 'y-or-n-p)
(column-number-mode 1)
(show-paren-mode 1)
(setq auto-save-default t)
(require 'paren)
(global-hl-line-mode +1)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; M-x magit-status
(message "==> package-install 'use-package")
(package-install 'use-package)

(use-package magit
  :ensure t
  :bind ("C-x g" . magit-status))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; pour faciliter l'evaluation des blocs python et sh
;; https://github.com/diadochos/org-babel-eval-in-repl
(require 'ob)
(require 'ob-python)
(org-babel-do-load-languages
 'org-babel-load-languages
 '(
   (shell . t)
   (python . t)
   )
 )

(use-package org-babel-eval-in-repl
  :ensure t
  :bind (
         (:map org-mode-map
               ("C-<right>" . ober-eval-block-in-repl) ;;; evalue tout le block
               ("S-<right>" . ober-eval-in-repl);;; evalue la ligne courante

               ("M-t" . org-babel-tangle) ;;; tangle tout le fichier
               ("C-*" . org-babel-execute-src-block) ;;; evalue le block+copie du résultat
               )
         )
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(load-theme 'tsdh-dark t)
